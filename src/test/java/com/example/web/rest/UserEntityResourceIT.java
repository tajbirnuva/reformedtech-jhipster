package com.example.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.example.IntegrationTest;
import com.example.domain.UserEntity;
import com.example.repository.UserEntityRepository;
import com.example.service.dto.UserEntityDTO;
import com.example.service.mapper.UserEntityMapper;
import jakarta.persistence.EntityManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link UserEntityResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UserEntityResourceIT {

    private static final String DEFAULT_FIRSTNAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRSTNAME = "BBBBBBBBBB";

    private static final String DEFAULT_LASTNAME = "AAAAAAAAAA";
    private static final String UPDATED_LASTNAME = "BBBBBBBBBB";

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_ROLES = "AAAAAAAAAA";
    private static final String UPDATED_ROLES = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/user-entities";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private UserEntityMapper userEntityMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserEntityMockMvc;

    private UserEntity userEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserEntity createEntity(EntityManager em) {
        UserEntity userEntity = new UserEntity()
            .firstname(DEFAULT_FIRSTNAME)
            .lastname(DEFAULT_LASTNAME)
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD)
            .email(DEFAULT_EMAIL)
            .roles(DEFAULT_ROLES);
        return userEntity;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserEntity createUpdatedEntity(EntityManager em) {
        UserEntity userEntity = new UserEntity()
            .firstname(UPDATED_FIRSTNAME)
            .lastname(UPDATED_LASTNAME)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .email(UPDATED_EMAIL)
            .roles(UPDATED_ROLES);
        return userEntity;
    }

    @BeforeEach
    public void initTest() {
        userEntity = createEntity(em);
    }

    @Test
    @Transactional
    void createUserEntity() throws Exception {
        int databaseSizeBeforeCreate = userEntityRepository.findAll().size();
        // Create the UserEntity
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);
        restUserEntityMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userEntityDTO))
            )
            .andExpect(status().isCreated());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeCreate + 1);
        UserEntity testUserEntity = userEntityList.get(userEntityList.size() - 1);
        assertThat(testUserEntity.getFirstname()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(testUserEntity.getLastname()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(testUserEntity.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testUserEntity.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testUserEntity.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testUserEntity.getRoles()).isEqualTo(DEFAULT_ROLES);
    }

    @Test
    @Transactional
    void createUserEntityWithExistingId() throws Exception {
        // Create the UserEntity with an existing ID
        userEntity.setId(1L);
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);

        int databaseSizeBeforeCreate = userEntityRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserEntityMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userEntityDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllUserEntities() throws Exception {
        // Initialize the database
        userEntityRepository.saveAndFlush(userEntity);

        // Get all the userEntityList
        restUserEntityMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstname").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastname").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].roles").value(hasItem(DEFAULT_ROLES)));
    }

    @Test
    @Transactional
    void getUserEntity() throws Exception {
        // Initialize the database
        userEntityRepository.saveAndFlush(userEntity);

        // Get the userEntity
        restUserEntityMockMvc
            .perform(get(ENTITY_API_URL_ID, userEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userEntity.getId().intValue()))
            .andExpect(jsonPath("$.firstname").value(DEFAULT_FIRSTNAME))
            .andExpect(jsonPath("$.lastname").value(DEFAULT_LASTNAME))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.roles").value(DEFAULT_ROLES));
    }

    @Test
    @Transactional
    void getNonExistingUserEntity() throws Exception {
        // Get the userEntity
        restUserEntityMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingUserEntity() throws Exception {
        // Initialize the database
        userEntityRepository.saveAndFlush(userEntity);

        int databaseSizeBeforeUpdate = userEntityRepository.findAll().size();

        // Update the userEntity
        UserEntity updatedUserEntity = userEntityRepository.findById(userEntity.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedUserEntity are not directly saved in db
        em.detach(updatedUserEntity);
        updatedUserEntity
            .firstname(UPDATED_FIRSTNAME)
            .lastname(UPDATED_LASTNAME)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .email(UPDATED_EMAIL)
            .roles(UPDATED_ROLES);
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(updatedUserEntity);

        restUserEntityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userEntityDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userEntityDTO))
            )
            .andExpect(status().isOk());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeUpdate);
        UserEntity testUserEntity = userEntityList.get(userEntityList.size() - 1);
        assertThat(testUserEntity.getFirstname()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testUserEntity.getLastname()).isEqualTo(UPDATED_LASTNAME);
        assertThat(testUserEntity.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testUserEntity.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testUserEntity.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUserEntity.getRoles()).isEqualTo(UPDATED_ROLES);
    }

    @Test
    @Transactional
    void putNonExistingUserEntity() throws Exception {
        int databaseSizeBeforeUpdate = userEntityRepository.findAll().size();
        userEntity.setId(longCount.incrementAndGet());

        // Create the UserEntity
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserEntityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userEntityDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userEntityDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchUserEntity() throws Exception {
        int databaseSizeBeforeUpdate = userEntityRepository.findAll().size();
        userEntity.setId(longCount.incrementAndGet());

        // Create the UserEntity
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserEntityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userEntityDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamUserEntity() throws Exception {
        int databaseSizeBeforeUpdate = userEntityRepository.findAll().size();
        userEntity.setId(longCount.incrementAndGet());

        // Create the UserEntity
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserEntityMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userEntityDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateUserEntityWithPatch() throws Exception {
        // Initialize the database
        userEntityRepository.saveAndFlush(userEntity);

        int databaseSizeBeforeUpdate = userEntityRepository.findAll().size();

        // Update the userEntity using partial update
        UserEntity partialUpdatedUserEntity = new UserEntity();
        partialUpdatedUserEntity.setId(userEntity.getId());

        partialUpdatedUserEntity
            .lastname(UPDATED_LASTNAME)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .email(UPDATED_EMAIL)
            .roles(UPDATED_ROLES);

        restUserEntityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserEntity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserEntity))
            )
            .andExpect(status().isOk());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeUpdate);
        UserEntity testUserEntity = userEntityList.get(userEntityList.size() - 1);
        assertThat(testUserEntity.getFirstname()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(testUserEntity.getLastname()).isEqualTo(UPDATED_LASTNAME);
        assertThat(testUserEntity.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testUserEntity.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testUserEntity.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUserEntity.getRoles()).isEqualTo(UPDATED_ROLES);
    }

    @Test
    @Transactional
    void fullUpdateUserEntityWithPatch() throws Exception {
        // Initialize the database
        userEntityRepository.saveAndFlush(userEntity);

        int databaseSizeBeforeUpdate = userEntityRepository.findAll().size();

        // Update the userEntity using partial update
        UserEntity partialUpdatedUserEntity = new UserEntity();
        partialUpdatedUserEntity.setId(userEntity.getId());

        partialUpdatedUserEntity
            .firstname(UPDATED_FIRSTNAME)
            .lastname(UPDATED_LASTNAME)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .email(UPDATED_EMAIL)
            .roles(UPDATED_ROLES);

        restUserEntityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserEntity.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserEntity))
            )
            .andExpect(status().isOk());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeUpdate);
        UserEntity testUserEntity = userEntityList.get(userEntityList.size() - 1);
        assertThat(testUserEntity.getFirstname()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testUserEntity.getLastname()).isEqualTo(UPDATED_LASTNAME);
        assertThat(testUserEntity.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testUserEntity.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testUserEntity.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUserEntity.getRoles()).isEqualTo(UPDATED_ROLES);
    }

    @Test
    @Transactional
    void patchNonExistingUserEntity() throws Exception {
        int databaseSizeBeforeUpdate = userEntityRepository.findAll().size();
        userEntity.setId(longCount.incrementAndGet());

        // Create the UserEntity
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserEntityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, userEntityDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userEntityDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchUserEntity() throws Exception {
        int databaseSizeBeforeUpdate = userEntityRepository.findAll().size();
        userEntity.setId(longCount.incrementAndGet());

        // Create the UserEntity
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserEntityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userEntityDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamUserEntity() throws Exception {
        int databaseSizeBeforeUpdate = userEntityRepository.findAll().size();
        userEntity.setId(longCount.incrementAndGet());

        // Create the UserEntity
        UserEntityDTO userEntityDTO = userEntityMapper.toDto(userEntity);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserEntityMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userEntityDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserEntity in the database
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteUserEntity() throws Exception {
        // Initialize the database
        userEntityRepository.saveAndFlush(userEntity);

        int databaseSizeBeforeDelete = userEntityRepository.findAll().size();

        // Delete the userEntity
        restUserEntityMockMvc
            .perform(delete(ENTITY_API_URL_ID, userEntity.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserEntity> userEntityList = userEntityRepository.findAll();
        assertThat(userEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
