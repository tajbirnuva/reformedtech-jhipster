export interface IUserEntity {
  id: number;
  firstname?: string | null;
  lastname?: string | null;
  username?: string | null;
  password?: string | null;
  email?: string | null;
  roles?: string | null;
}

export type NewUserEntity = Omit<IUserEntity, 'id'> & { id: null };
