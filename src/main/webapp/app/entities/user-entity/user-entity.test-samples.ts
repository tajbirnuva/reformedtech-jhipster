import { IUserEntity, NewUserEntity } from './user-entity.model';

export const sampleWithRequiredData: IUserEntity = {
  id: 25097,
};

export const sampleWithPartialData: IUserEntity = {
  id: 15642,
  firstname: 'renounce',
  username: 'cardboard unless boo',
};

export const sampleWithFullData: IUserEntity = {
  id: 20002,
  firstname: 'considering large',
  lastname: 'faraway because',
  username: 'amaze near loyally',
  password: 'knottily ah kill',
  email: 'Jaime.Ledner0@yahoo.com',
  roles: 'quirkily hmph cluttered',
};

export const sampleWithNewData: NewUserEntity = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
