package com.example.service;

import com.example.domain.UserEntity;
import com.example.repository.UserEntityRepository;
import com.example.service.dto.UserEntityDTO;
import com.example.service.mapper.UserEntityMapper;
import jakarta.ws.rs.core.Response;
import java.util.*;
import java.util.stream.Collectors;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.example.domain.UserEntity}.
 */
@Service
@Transactional
public class UserEntityService {

    private final Logger log = LoggerFactory.getLogger(UserEntityService.class);

    private final UserEntityRepository userEntityRepository;

    private final UserEntityMapper userEntityMapper;

    private final Keycloak keycloak;

    @Value("${realm}")
    private String realm;

    public UserEntityService(UserEntityRepository userEntityRepository, UserEntityMapper userEntityMapper, Keycloak keycloak) {
        this.userEntityRepository = userEntityRepository;
        this.userEntityMapper = userEntityMapper;
        this.keycloak = keycloak;
    }

    /**
     * Save a userEntity.
     *
     * @param userEntityDTO the entity to save.
     * @return the persisted entity.
     */
    public UserEntityDTO save(UserEntityDTO userEntityDTO) {
        log.debug("Request to save UserEntity : {}", userEntityDTO);
        UserEntity userEntity = userEntityMapper.toEntity(userEntityDTO);
        userEntity = userEntityRepository.save(userEntity);
        return userEntityMapper.toDto(userEntity);
    }

    /**
     * Update a userEntity.
     *
     * @param userEntityDTO the entity to save.
     * @return the persisted entity.
     */
    public UserEntityDTO update(UserEntityDTO userEntityDTO) {
        log.debug("Request to update UserEntity : {}", userEntityDTO);
        UserEntity userEntity = userEntityMapper.toEntity(userEntityDTO);
        userEntity = userEntityRepository.save(userEntity);
        return userEntityMapper.toDto(userEntity);
    }

    /**
     * Partially update a userEntity.
     *
     * @param userEntityDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<UserEntityDTO> partialUpdate(UserEntityDTO userEntityDTO) {
        log.debug("Request to partially update UserEntity : {}", userEntityDTO);

        return userEntityRepository
            .findById(userEntityDTO.getId())
            .map(existingUserEntity -> {
                userEntityMapper.partialUpdate(existingUserEntity, userEntityDTO);

                return existingUserEntity;
            })
            .map(userEntityRepository::save)
            .map(userEntityMapper::toDto);
    }

    /**
     * Get all the userEntities.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<UserEntityDTO> findAll() {
        log.debug("Request to get all UserEntities");
        return userEntityRepository.findAll().stream().map(userEntityMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one userEntity by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UserEntityDTO> findOne(Long id) {
        log.debug("Request to get UserEntity : {}", id);
        return userEntityRepository.findById(id).map(userEntityMapper::toDto);
    }

    /**
     * Delete the userEntity by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete UserEntity : {}", id);
        userEntityRepository.deleteById(id);
    }

    /*---------------Keycloak METHOD---------------*/
    public Response createUserInKeycloak(UserEntityDTO dto) {
        UserRepresentation userRepresentation = mapUserEntityToUserRepresentation(dto);
        return keycloak.realm(realm).users().create(userRepresentation);
    }

    public void updateUserInKeycloak(UserEntityDTO dto) {
        List<UserRepresentation> userRepresentations = keycloak.realm(realm).users().searchByUsername(dto.getUsername(), true);
        UserRepresentation userRepresentation = mapUserEntityToUserRepresentation(dto);
        keycloak.realm(realm).users().get(userRepresentations.get(0).getId()).update(userRepresentation);
    }

    public List<UserEntityDTO> getKeycloakUsers() {
        List<UserRepresentation> list = keycloak.realm(realm).users().list();
        List<UserEntityDTO> userEntityDTOList = new ArrayList<>();
        for (UserRepresentation userRepresentation : list) {
            UserEntityDTO userEntityDTO = new UserEntityDTO();
            userEntityDTO.setFirstname(userRepresentation.getFirstName());
            userEntityDTO.setLastname(userRepresentation.getLastName());
            userEntityDTO.setEmail(userRepresentation.getEmail());
            userEntityDTO.setUsername(userRepresentation.getUsername());
            userEntityDTOList.add(userEntityDTO);
        }
        return userEntityDTOList;
    }

    /*---------------HELPER METHOD---------------*/
    public boolean usernameExistInKeycloak(String username) {
        List<UserRepresentation> userRepresentations = keycloak.realm(realm).users().searchByUsername(username, true);
        return !userRepresentations.isEmpty();
    }

    public boolean emailExistInKeycloak(String email) {
        List<UserRepresentation> userRepresentations = keycloak.realm(realm).users().searchByEmail(email, true);
        return !userRepresentations.isEmpty();
    }

    private UserRepresentation mapUserEntityToUserRepresentation(UserEntityDTO dto) {
        UserRepresentation userRep = new UserRepresentation();
        userRep.setCreatedTimestamp(1L);
        userRep.setUsername(dto.getUsername());
        userRep.setEmail(dto.getEmail());
        userRep.setEmailVerified(true);
        userRep.setEnabled(true);
        userRep.setFirstName(dto.getFirstname());
        userRep.setLastName(dto.getLastname());
        CredentialRepresentation passwordCred = new CredentialRepresentation();
        passwordCred.setTemporary(false);
        passwordCred.setType(CredentialRepresentation.PASSWORD);
        passwordCred.setValue(dto.getPassword());

        userRep.setCredentials(Arrays.asList(passwordCred));

        return userRep;
    }
}
