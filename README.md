# Application Setup instructions for developers

## Environmental Dependency To Run This Project

Before you can build this project, you must install and configure the following dependencies on your machine:

- Node js v18.18.2
- JDK 17.0.9
- Keycloak 22.0.5
- Jhipster 8.0.0

## Keycloak configuration file configure

Configure File destination : `keycloak-21.0.0\conf\keycloak.conf`

```
# Database
db=mysql
db-username=root
db-password=1234
db-url=jdbc:mysql://localhost:3306/keycloakdb

#Application Port
http-port=8081
```

## Keycloak setup to manage user identities and roles.

### Create New Realm

- Provide `Realm Name` : reformedtech
- Make `Enabled` On

### Create New Client

- `Client type` : OpenID Connect
- `Client Id` : reformdtech_client
- `Client Name` : Reformdtech Client
- `Description` : Reformdtech Client
- `Client authentication` : On
- `Authentication flow` : Select `Standard Flow` & `Direct access Grants`
- `Root URL` : http://localhost:8080
- `Valid redirect URIs` :
  - http://localhost:/\*
  - https://localhost:/\*
  - http://127.0.0.1:/*
  - https://127.0.0.1:/*
- `Web Origins` : \*

> Note : For Keycloak 22.0.5 or higher use only \* for Valid redirect URIs

#### Create internal Client Scopes for the Created Client

Select Created client (reformdtech_client) from `Clients list`. Select `reformdtech_client-dedicated`. select `Configure a new mapper`

- Select Mapper Type `User Realm Role`
- `Name` : roles
- `Multivalued` : On
- `Token Claim Name` : roles
- `Claim JSON Type` : String
- `Add to access token` : On
- `Add of Userinfo` : On

**_Hints : This Client Scope Mapper helps for Role Base Authorization_**

### Create Client Scope

- `Name` : reformedtech
- `Description` : Reformed Tech
- `Protocol` : OpenID Connect
- `Display on consent screen` : Off
- `Include in token scope` : On

### Create 3 new Mappers for Created Client Scope

Select the Created Client Scope then select `Configure a new mapper`

#### 1. Mapper Type `User Attribute`

- `Name` : login
- `User Attribute` : preferred_username
- `Token Claim Name` : login
- `Claim JSON Type` : String
- `Add of Userinfo` : On

#### 2. Mapper Type `User Attribute`

- `Name` : langkey
- `User Attribute` : langkey
- `Token Claim Name` : langkey
- `Claim JSON Type` : String
- `Add of Userinfo` : On

#### 3. Mapper Type `User Realm Role`

- `Name` : roles
- `Multivalued` : On
- `Token Claim Name` : roles
- `Claim JSON Type` : String
- `Add to access token` : On
- `Add of Userinfo` : On

### Create Users

- `Username` : tajbir_admin
- `Email` : admin@localhost.com
- `First Name` :Tajbir
- `Last Name` : Admin
- Create credintials for users
  In Similar way please create another user `tajbir_user`

**_Hints : Please fill up all the fields otherwise we can not login in application because of null vaule exception_**

### Create Roles

Create two roles named `ROLE_ADMIN` and `ROLE_USER`

- Use `ROLE_` as prefix in role name
- All letters are capital. Ex: `ROLE_ADMIN`

### Create Groups

Create two Groups named `Admins` and `Users`

- Add created user from `Member`
- Assign created role from `Role Mapping`

### Create Realm-Admin Level User

For internal communication between Keycloak and Springboot Application to fetch keycloak data

- `Username` : keycloak_admin
- `Email` : keycloak_admin@localhost.com
- `First Name` : keycloak
- `Last Name` : Admin
- Assign 'realm-admin' provided by `realm-management` from `Role mapping`
- Create credintials for users

## Configure Spring Boot for keycloak

File destination : `src/main/resources/config/application.yml`

```yaml
spring:
  ...
  security:
    oauth2:
      client:
        provider:
          oidc:
            issuer-uri: http://localhost:8081/realms/reformedtech
        registration:
          oidc:
            client-id: reformdtech_client
            client-secret: oOMRoV7T59ODjTXwIe1B4VBkLClYxIco
            scope: openid, profile, email, offline_access
```

```
# Keycloak client,User and url for SpringBoot Application to fetch data from Keycloak:
realm: reformedtech
server-url: http://localhost:8081
client-id: admin-cli
grant-type: password
name: keycloak_admin
password: 12345
```

## Configure Spring Boot for Database

File destination : `src/main/resources/config/application-dev.yml`

```yaml
spring:
  ...
  url:jdbc: mysql://localhost:3306/reformedtechdb?useUnicode=true&characterEncoding=utf8&useSSL=false&useLegacyDatetimeCode=false&createDatabaseIfNotExist=true
  username: root
  password: 1234
```

## Users, Their Roles and Access

| Username     | Password | Role  | Access Point                             |
| ------------ | -------- | ----- | ---------------------------------------- |
| tajbir_user  | tajbir   | User  | User Update                              |
| tajbir_admin | tajbir   | Admin | User Create, Delete, Keycloak Users List |

**_Hints : Both Users can see the all created user List_**

## Application Run

- Run Keycloak by `kc.bat start-dev` command
- Run Frontend by `npm start` command
- Run Backend by `./mvnw` command or intellij idea.
